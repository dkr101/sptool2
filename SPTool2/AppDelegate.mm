//
//  AppDelegate.m
//  SPTool2
//
//  Created by David Robinson on 2/24/13.
//  Copyright (c) 2013 BCH Technologies. All rights reserved.
//

#import "AppDelegate.h"
#import "MyImageView.h"
#import <iostream>
#import <vector>

#import "graham_scan.h"

#import "PointToStringTransformer.h"


using namespace std;


static const float kPixelsPerMeter = 32.f;


static inline NSPoint PTM(const NSPoint pixels) {
    
    NSPoint meters;
    meters.x = pixels.x / kPixelsPerMeter;
    meters.y = pixels.y / kPixelsPerMeter;

    return meters;
}






@interface AppDelegate ()
@property (weak) IBOutlet NSTextField *anchorPointXTextField;
@property (weak) IBOutlet NSTextField *anchorPointYTextField;

@property (weak) IBOutlet MyImageView *imageView;
@property (unsafe_unretained) IBOutlet NSTextView *outputTextView;
@end

@implementation AppDelegate

#pragma mark - Set Up

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    
    [self initPointToStringTransformer];
}


- (void)initPointToStringTransformer {
    
    // Create and register the point to string transformer.
    
    PointToStringTransformer *ptToStrTransformer = [PointToStringTransformer new];
    [NSValueTransformer setValueTransformer:ptToStrTransformer
                                    forName:@"PointToStringTransformer"];
}


#pragma mark - Printing Points

- (IBAction)printImagePoints:(id)sender {
    
    NSMutableString *output = [NSMutableString stringWithString:@"Image Points\n\n"];
    
    for (NSValue *value in (NSArray *)_imagePoints.content) {
        
        NSPoint pt = value.pointValue;
        
        [output appendFormat:@"%f,%f\n", pt.x, pt.y];
    }
    
    _outputTextView.string = output;
}

- (IBAction)printBox2DPoints:(id)sender {

    NSMutableString *output = [NSMutableString stringWithString:@"Box2D Points\n\n"];
    
    NSPoint centerPt = _imageView.centerPoint;
    
    NSArray *pts = _imagePoints.content;
    
    for (NSValue *value in pts) {
        
        NSPoint imgPt = value.pointValue;
        
        imgPt.x -= centerPt.x;
        imgPt.y -= centerPt.y;
        
        NSPoint b2Pt = PTM(imgPt);
        
        [output appendFormat:@"{%f,%f},\n", b2Pt.x, b2Pt.y];
    }

    [output appendFormat:@"\n%lu points\n", (unsigned long)[pts count]];
    
    _outputTextView.string = output;
}


#pragma mark - Point Manipulation

- (IBAction)clearPoints:(id)sender {
    
    [_imagePoints removeObjects:[_imagePoints content]];
    [_imageView clearPoints];
}

- (IBAction)cullToConvexHull:(id)sender {
    
    cout << endl;
    cout << "Cull to Convex Hull\n";
    
    NSMutableArray *pts = _imagePoints.content;
    
    // Set up a vector of points.
    
    vector<CGPoint> cgPoints;
    cgPoints.reserve(pts.count);
    
    for (NSValue *value in pts)
        cgPoints.push_back(static_cast<CGPoint>(value.pointValue));
    
    // Graham scan the vector to get the convex hull.
    
    size_t numPts = grahamScan(&cgPoints[0], cgPoints.size());
    cgPoints.resize(numPts);

    // Replace the image points array.
    
    [pts removeAllObjects];

    for (const auto& pt : cgPoints) {
        
        NSValue *value = [NSValue valueWithPoint:static_cast<NSPoint>(pt)];
        [_imagePoints addObject:value];
    }

    [_imageView refreshPoints];
    
    
    
    cout << "Number of image points: " << pts.count << endl;
}
- (IBAction)alignCenterPoint:(id)sender {
    [_imageView alignCenterPoint];
}

- (IBAction)flipX:(id)sender {
    
//    cout << endl;
//    cout << "Flip X\n";
    
    [_imageView flipAboutX];
}


#pragma mark - Document

- (void)openDocument:(id)sender {
    
	// Create and configure the panel.
	
	NSOpenPanel *panel = [NSOpenPanel openPanel];
	panel.allowedFileTypes = @[ @"png", @"jpg", @"jpeg" ];
	panel.allowsMultipleSelection = NO;
	panel.canChooseDirectories = NO;
	panel.canChooseFiles = YES;
	
	// Run the panel.
	
	[panel beginSheetModalForWindow:self.window
				  completionHandler:^(NSInteger result)
     {
         //NSLog(@"completionHandler called with %d", returnCode);
         
         if (result == NSOKButton) {
             
             NSURL *url = panel.URL;
             NSString *ext = url.pathExtension;
             
             if ([ext isEqualToString:@"png"] ||
                 [ext isEqualToString:@"jpg"]) {
                 
                 _imageView.image = [[NSImage alloc] initWithContentsOfURL:url];
                 
             }
             else {
                 NSException *exception = [NSException exceptionWithName:@"Bad File Type"
                                                                  reason:@"Not an image file"
                                                                userInfo:nil];
                 @throw exception;
             }
             
             
         }
     }];
}

@end
