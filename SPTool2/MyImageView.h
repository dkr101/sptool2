//
//  MyImageView.h
//  SPTool1
//
//  Created by David Robinson on 2/23/13.
//  Copyright (c) 2013 BCH Technologies. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class PointView;

@interface MyImageView : NSView
- (void)clearPoints;
- (void)refreshPoints;

@property(nonatomic, strong) NSImage *image;
/*
 The actual size of the image in the view.
 */
@property(nonatomic, readonly) NSSize scaledImgSize;


@property(nonatomic, readonly) NSPoint centerPoint;
@property(nonatomic, readonly) NSPoint anchorPoint;

@property(nonatomic) CGFloat apX, apY;

- (void)updateNormPosAndImgPtForPointView:(PointView *)ptView;
- (void)removePointView:(PointView *)ptView;

- (void)flipAboutX;

- (void)alignCenterPoint;
@end
