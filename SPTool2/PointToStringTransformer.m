//
//  PointToStringTransformer.m
//  SPTool2
//
//  Created by David Robinson on 7/17/13.
//  Copyright (c) 2013 BCH Technologies. All rights reserved.
//

#import "PointToStringTransformer.h"

@implementation PointToStringTransformer

+ (Class)transformedValueClass {
    return [NSString class];
}

+ (BOOL)allowsReverseTransformation {
    return NO;
}

- (id)transformedValue:(id)value {
    
    if (value == nil)
        return nil;
    
    /*
     Attempt to get a reasonable value from the
     value object.
     */
    if (![value respondsToSelector: @selector(pointValue)]) {
        
        [NSException raise: NSInternalInconsistencyException
                    format: @"Value (%@) does not respond to -pointValue.",
         [value class]];
    }

    NSPoint pt = [value pointValue];
    
    return [NSString stringWithFormat:@"%f,%f", pt.x, pt.y];
}

@end
