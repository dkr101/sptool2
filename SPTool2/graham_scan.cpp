//
//  graham_scan.cpp
//  SPTool2
//
//  Created by David Robinson on 6/26/13.
//  Copyright (c) 2013 BCH Technologies. All rights reserved.
//

#include <CoreGraphics/CoreGraphics.h>

#include <iostream>
#include <vector>
#include <algorithm>


#include "graham_scan.h"


using namespace std;

/*
 Three points are a counter-clockwise turn if ccw > 0, clockwise if
 ccw < 0, and collinear if ccw = 0 because ccw is a determinant that
 gives the signed area of the triangle formed by p1, p2 and p3.
 */
static float ccw(const CGPoint& p1, const CGPoint& p2, const CGPoint& p3) {
    return (p2.x - p1.x)*(p3.y - p1.y) - (p2.y - p1.y)*(p3.x - p1.x);
}

static size_t rightMostBottomCoordIndex(const CGPoint *points, size_t N) {
    
    size_t extreme = 0;
    
    for (size_t i = 0; i < N; ++i) {
        
        if (points[i].y < points[extreme].y)
            extreme = i;
        else if (points[i].y == points[extreme].y) {
            if (points[i].x > points[extreme].x)
                extreme = i;
        }
    }
    
    return extreme;
}


static void sortByPolarAngle(vector<CGPoint>& points) {
    
    const CGPoint& origin = points[1];
    
    struct PtInfo {
        CGPoint pt_;
        float angle_;
        
        bool operator<(const PtInfo& rhs) const {
            return angle_ < rhs.angle_;
        }
    };
    
    vector<PtInfo> infos(points.size());
    
    
    for (size_t i = 2; i < infos.size(); ++i) {
        
        PtInfo& pi = infos[i];
        
        // Save the point.
        
        pi.pt_ = points[i];
        
        // Compute the angle made with points[1].
        
        float x = pi.pt_.x - origin.x;
        float y = pi.pt_.y - origin.y;
        pi.angle_ = atan2f(y, x);
    }
    
    sort(infos.begin() + 2, infos.end());
    
    // Copy it back out.
    
    for (size_t i = 2; i < points.size(); ++i)
        points[i] = infos[i].pt_;
    
    // Debug output.
    
    cout << "Polar angle sort results:\n";
    for (size_t i = 1; i < infos.size(); ++i) {
        cout << points[i].x << "," << points[i].y << ": angle = " << infos[i].angle_ << endl;
    }
    
}


size_t grahamScan(CGPoint *pts, size_t N) {
    
    vector<CGPoint> points; // array of points.  Points are at indices 1..N.
    points.reserve(N + 1);
    points.push_back(CGPointMake(0,0));
    points.insert(points.end(), pts, pts + N);
    
    
    // Swap points[1] with the point with the lowest y-coordinate.
    
    size_t lowestY = rightMostBottomCoordIndex(&points[1], N) + 1;
    std::swap(points[1], points[lowestY]);
    
    // Sort points by polar angle with points[1].
    
    sortByPolarAngle(points);
    
    
    // We want points[0] to be a sentinel point that will stop the loop.
    
    points[0] = points[N];
    
    // M will denote the number of points on the convex hull.
    
    size_t M = 1;
    for (size_t i = 2; i <= N; ++i) {
        
        // Find next valid point on convex hull.
        
        while (ccw(points[M-1], points[M], points[i]) <= 0) {
            
            if (M > 1)
                --M;
            
            else if (i == N) // all points are colinear
                break;
            
            else
                ++i;
        }
        
        // Update M and swap points[i] to the correct place.
        
        ++M;
        std::swap(points[M], points[i]);
    }
    
    memcpy(pts, &points[0], M * sizeof(points[0]));
    
    
    return M;
}
