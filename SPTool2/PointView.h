//
//  PointView.h
//  SPTool2
//
//  Created by David Robinson on 2/25/13.
//  Copyright (c) 2013 BCH Technologies. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface PointView : NSView

- (id)initWithViewPosition:(NSPoint)viewPos
        normalizedPosition:(NSPoint)normPos;

@property(nonatomic, strong) NSColor *color;
@property(nonatomic, strong) NSValue *imagePointValue;

/*
 The position within the image, scaled from 0 to 1.
 */
@property(nonatomic, assign) NSPoint normalizedPosition;

- (void)repositionWithinSize:(NSSize)size;
@end
