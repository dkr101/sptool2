//
//  MyImageView.m
//  SPTool1
//
//  Created by David Robinson on 2/23/13.
//  Copyright (c) 2013 BCH Technologies. All rights reserved.
//

#import "MyImageView.h"
#import "AppDelegate.h"
#import "PointView.h"

@interface MyImageView()

@property(nonatomic, assign) BOOL flipX;

@property(nonatomic, readwrite) NSSize scaledImgSize;

@property(nonatomic, strong) PointView *centerPtView;
@property(nonatomic, strong) NSMutableSet *ptViews;

@end



@implementation MyImageView

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {

        self.ptViews = [NSMutableSet new];
        _scaledImgSize = self.frame.size;
        _flipX = NO;
    }
    
    return self;
}


- (id)initWithFrame:(NSRect)frameRect {
    
    self = [super initWithFrame:frameRect];
    if (self) {
        
        self.ptViews = [NSMutableSet new];
        _scaledImgSize = frameRect.size;
        _flipX = NO;
    }
    return self;
}




- (void)drawRect:(NSRect)dirtyRect {
    
    NSGraphicsContext *context = [NSGraphicsContext currentContext];
    
    [context saveGraphicsState];
    
    if (_flipX) {
        
        const CGFloat x = _scaledImgSize.width;
        
        NSAffineTransform *transform = [NSAffineTransform transform];
        [transform translateXBy:x yBy:0.f];
        [transform scaleXBy:-1.f yBy:1.f];
        
        [transform concat];
    }

    
    [_image drawInRect:NSMakeRect(0, 0, _scaledImgSize.width, _scaledImgSize.height)
              fromRect:NSZeroRect
             operation:NSCompositeSourceOver
              fraction:1];
    
    [context restoreGraphicsState];
}

- (void)setImage:(NSImage *)image {
    
    if (image == _image)
        return;
    
    _image = image;
    
    if (_image)
        _scaledImgSize = [self scaledImageSizeForViewSize:self.frame.size];
    
    [self setNeedsDisplay:YES];
}

- (void)resizeSubviewsWithOldSize:(NSSize)oldBoundsSize {
    
//    NSLog(@"view size: %f, %f", self.frame.size.width, self.frame.size.height);
    
    [super resizeSubviewsWithOldSize:oldBoundsSize];
    
    for (PointView *ptView in _ptViews) {
        [ptView repositionWithinSize:_scaledImgSize];
    }
    
    if (_centerPtView) {
        [_centerPtView repositionWithinSize:_scaledImgSize];
    }
}

- (void)setFrameSize:(NSSize)newSize {

    [super setFrameSize:newSize];
//    NSLog(@"new size: %f,%f", newSize.width, newSize.height);
    
    if (_image)
        _scaledImgSize = [self scaledImageSizeForViewSize:newSize];
}

- (NSSize)scaledImageSizeForViewSize:(NSSize)viewSize {
    
    // Get the aspect ratio of the image.
    
    NSSize imgSize = _image.size;
    CGFloat ai = imgSize.width  / imgSize.height;
    
    // Get the aspect ratio of the view.
    
    CGFloat av = viewSize.width / viewSize.height;
    
    /*
     Set one of the scaled image dimensions to one of the view dimensions, depending on which aspect ratio is greater.
     Compute the other dimension using the image's aspect ratio.
     */
    return av < ai ? NSMakeSize(viewSize.width, viewSize.width / ai) : NSMakeSize(viewSize.height * ai, viewSize.height);
}


- (void)mouseDown:(NSEvent *)theEvent {

    NSUInteger modFlags = theEvent.modifierFlags;
    
    BOOL pickingCenter = (modFlags & NSControlKeyMask) != 0;
    
    if (!_image)
        return;
    
    
    // Compute the position in the view and normalize it.

    NSPoint viewPos = [self convertPoint:theEvent.locationInWindow
                           fromView:nil];
    NSPoint normPos = [self normPosForViewPos:viewPos];
    
    
    // Create the point view and add it.

    PointView *ptView = [[PointView alloc] initWithViewPosition:viewPos
                                             normalizedPosition:normPos];
    [self addSubview:ptView];
    
    
    
    if (!pickingCenter) {
        
        [_ptViews addObject:ptView];
        
        // Create an image point value.
        
        const NSPoint imgPt = [self imgPosForNormPos:normPos];
        NSValue *imgPtValue = [NSValue valueWithPoint:imgPt];
        
        // Link it to the new point view.
        
        ptView.imagePointValue = imgPtValue;
        
        // Add it to the image points collection.
        
        AppDelegate *ad = [NSApplication sharedApplication].delegate;
        [ad.imagePoints addObject:imgPtValue];
        
        ptView.color = [NSColor greenColor];
    }
    else {
        [self notifyCenterAndAnchorPointsWillChange];

        // Replace the center point view.
        
        [_centerPtView removeFromSuperview];
        self.centerPtView = ptView;
        ptView.color = [NSColor blueColor];
        
        [self notifyCenterAndAnchorPointsDidChange];
    }
}

- (void)notifyCenterAndAnchorPointsWillChange {
    [self willChangeValueForKey:@"centerPoint"];
    [self willChangeValueForKey:@"anchorPoint"];
    [self willChangeValueForKey:@"apX"];
    [self willChangeValueForKey:@"apY"];
}
- (void)notifyCenterAndAnchorPointsDidChange {
    [self didChangeValueForKey:@"centerPoint"];
    [self didChangeValueForKey:@"anchorPoint"];
    [self didChangeValueForKey:@"apX"];
    [self didChangeValueForKey:@"apY"];
}


- (void)clearPoints {
    
    for (PointView *ptView in _ptViews)
        [ptView removeFromSuperview];
    
    [_ptViews removeAllObjects];
}


- (void)refreshPoints {
    
    AppDelegate *ad = [NSApplication sharedApplication].delegate;
    NSArray *imgPts = ad.imagePoints.content;
    
    NSMutableSet *toRemove = [NSMutableSet new];
    
    for (PointView *ptView in _ptViews) {
        
        if (![imgPts containsObject:ptView.imagePointValue]) {
            [ptView removeFromSuperview];
            [toRemove addObject:ptView];
        }
    }
    
    [_ptViews minusSet:toRemove];
}

/*
 Returns a position within the unscaled image.
 */
- (NSPoint)imgPosForNormPos:(NSPoint)normPos {
    
    NSSize imgSize = _image.size;
    return NSMakePoint(normPos.x * imgSize.width, normPos.y * imgSize.height);
}

- (NSPoint)normPosForViewPos:(NSPoint)viewPos {
    
    return NSMakePoint(viewPos.x/_scaledImgSize.width,
                       viewPos.y/_scaledImgSize.height);
}

- (NSPoint)centerPoint {
    
    if (_centerPtView) {
        
       return [self imgPosForNormPos:_centerPtView.normalizedPosition];
    }
    else {
        
        return NSMakePoint(0.f, 0.f);
    }
}

- (NSPoint)anchorPoint {
    
    if (_centerPtView) {
        
        return _centerPtView.normalizedPosition;
    }
    else {
        
        return NSMakePoint(0.f, 0.f);
    }
}

- (CGFloat)apX {
    return [self anchorPoint].x;
}
- (CGFloat)apY {
    return [self anchorPoint].y;
}
- (void)setApX:(CGFloat)apX {
    
    CGPoint ap = [self anchorPoint];
    ap.x = apX;
    
    [self updateCenterPointViewWithAnchorPoint:ap];
}

- (void)setApY:(CGFloat)apY {
    
    CGPoint ap = [self anchorPoint];
    ap.y = apY;
    
    [self updateCenterPointViewWithAnchorPoint:ap];
}

- (void)updateCenterPointViewWithAnchorPoint:(CGPoint)ap {
    
    [self notifyCenterAndAnchorPointsWillChange];
    
    _centerPtView.normalizedPosition = ap;
    [_centerPtView repositionWithinSize:_scaledImgSize];
    
    [self notifyCenterAndAnchorPointsDidChange];
}




static inline NSPoint rectCenter(const NSRect rect) {
    return NSMakePoint(rect.origin.x + 0.5f*rect.size.width, rect.origin.y  + 0.5f*rect.size.height);
}

- (void)updateNormPosAndImgPtForPointView:(PointView *)ptView {
    
    if (ptView == _centerPtView)
        [self notifyCenterAndAnchorPointsWillChange];
    /*
     Compute a normalized position from the point view's position
     within its parent.
     */
    NSPoint viewPos = rectCenter(ptView.frame);
    ptView.normalizedPosition = [self normPosForViewPos:viewPos];
    
    if (ptView != _centerPtView)    
        [self changeImgPtValueForPtView:ptView];
    
    else
        [self notifyCenterAndAnchorPointsDidChange];
}

- (void)removePointView:(PointView *)ptView {
    
    [ptView removeFromSuperview];
    
    if (ptView != _centerPtView) {
    
        [_ptViews removeObject:ptView];
        
        // Remove the image point value.
    
        AppDelegate *ad = [NSApplication sharedApplication].delegate;
        [ad.imagePoints removeObject:ptView.imagePointValue];
        
    }
    else {
        
        [self notifyCenterAndAnchorPointsWillChange];
    
        _centerPtView = nil;
        
        [self notifyCenterAndAnchorPointsDidChange];
    }
}


- (void)flipXForPtView:(PointView *)ptView {

    // Compute a normalized position.
    
    NSPoint normPos = ptView.normalizedPosition;
    normPos.x = -normPos.x + 1.f;
    ptView.normalizedPosition = normPos;
    
    // Update the view position.
    
    NSPoint viewPos = rectCenter(ptView.frame);
    viewPos.x = -viewPos.x + _scaledImgSize.width;
    NSRect f = ptView.frame;
    f.origin.x = viewPos.x - 0.5f*f.size.width;
    ptView.frame = f;
    
    if (ptView != _centerPtView) {
        [self changeImgPtValueForPtView:ptView];
    }
}


- (void)changeImgPtValueForPtView:(PointView *)ptView {
    
    // Remove the old image point value.
    
    AppDelegate *ad = [NSApplication sharedApplication].delegate;
    [ad.imagePoints removeObject:ptView.imagePointValue];
    
    // Create a new image point value.
    
    NSPoint imgPt = [self imgPosForNormPos:ptView.normalizedPosition];
    ptView.imagePointValue = [NSValue valueWithPoint:imgPt];
    
    // Add it to the image point collection.
    
    [ad.imagePoints addObject:ptView.imagePointValue];
}

- (void)flipAboutX {
    
    _flipX = !_flipX;
    
    for (PointView *ptView in _ptViews)
        [self flipXForPtView:ptView];
    
    if (_centerPtView) {
     
        [self notifyCenterAndAnchorPointsWillChange];
        
        [self flipXForPtView:_centerPtView];
        
        [self notifyCenterAndAnchorPointsDidChange];
    }
    
    self.needsDisplay = YES;
}


- (void)alignCenterPoint {
    
    // Compute the normalized position and position within the view.
    
    NSPoint normPos = NSMakePoint(0.5f, 0.5f);
    NSPoint viewPos = NSMakePoint(0.5f * _scaledImgSize.width, 0.5f * _scaledImgSize.height);
    
    // Replace the center point view.
    
    [self notifyCenterAndAnchorPointsWillChange];
    
    [_centerPtView removeFromSuperview];
    self.centerPtView = [[PointView alloc] initWithViewPosition:viewPos
                                             normalizedPosition:normPos];
    _centerPtView.color = [NSColor blueColor];
    [self addSubview:_centerPtView];
    
    [self notifyCenterAndAnchorPointsDidChange];
}


@end
