//
//  PointView.m
//  SPTool2
//
//  Created by David Robinson on 2/25/13.
//  Copyright (c) 2013 BCH Technologies. All rights reserved.
//

#import "PointView.h"
#import "MyImageView.h"


static const CGFloat kSize = 8.f;

@interface PointView () {
    BOOL tracking;
    NSPoint prevWindowPos;
}
@end


static inline NSRect rectCenteredAtPoint(const NSPoint pt) {
    return NSMakeRect(pt.x - 0.5f*kSize, pt.y - 0.5f*kSize, kSize, kSize);
}


@implementation PointView

- (id)initWithViewPosition:(NSPoint)viewPos
        normalizedPosition:(NSPoint)normPos
{
    self = [super initWithFrame:rectCenteredAtPoint(viewPos)];
    if (self) {
        self.color = [NSColor whiteColor];
        self.normalizedPosition = normPos;
        tracking = NO;
    }

    return self;
}

- (void)drawRect:(NSRect)rect
{
    [_color set];
    NSRectFill(rect);
}


- (void)rightMouseDown:(NSEvent *)theEvent {
    
    MyImageView *imgView = (MyImageView *)self.superview;
    [imgView removePointView:self];
}


- (void)mouseDown:(NSEvent *)theEvent {
    
    prevWindowPos = theEvent.locationInWindow;
    
    tracking = YES;

//    NSLog(@"mouse down");
}

- (void)mouseDragged:(NSEvent *)theEvent {
    
    if (tracking) {
        
        const NSPoint curWindowPos = theEvent.locationInWindow;
        
        // Compute the displacement.
        
        const CGFloat dx = curWindowPos.x - prevWindowPos.x;
        const CGFloat dy = curWindowPos.y - prevWindowPos.y;
        
        // Get the frame.
        
        NSRect f = self.frame;
        
        // Mark the old frame area for redrawing.
        
        [[self superview] setNeedsDisplayInRect:f];
        
        // Move the frame by the displacement.
        
        f.origin.x += dx;
        f.origin.y += dy;
        self.frame = f;
        
        // Update the previous window position for the next iteration.
        
        prevWindowPos = curWindowPos;
    }
    
//    NSLog(@"mouse dragged");
}

- (void)mouseUp:(NSEvent *)theEvent {

    MyImageView *imgView = (MyImageView *)self.superview;

    [imgView updateNormPosAndImgPtForPointView:self];
    
    tracking = NO;
//    NSLog(@"mouse up");
}

- (void)repositionWithinSize:(NSSize)size {
    
    if (tracking)
        return;
    
    NSPoint newCenter = NSMakePoint(_normalizedPosition.x * size.width,
                             _normalizedPosition.y * size.height);
    self.frame = rectCenteredAtPoint(newCenter);
}



@end
