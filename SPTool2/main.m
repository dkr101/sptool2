//
//  main.m
//  SPTool2
//
//  Created by David Robinson on 2/24/13.
//  Copyright (c) 2013 BCH Technologies. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
