//
//  graham_scan.h
//  SPTool2
//
//  Created by David Robinson on 6/26/13.
//  Copyright (c) 2013 BCH Technologies. All rights reserved.
//

#pragma once


/*
 
 N = number of points
 return = M, the number of points on the hull.
 
 */
size_t grahamScan(CGPoint *pts, size_t N);